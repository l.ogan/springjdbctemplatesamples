# Spring JdbcTemplate Sample Application

Spring Boot Application build using Maven. Assumption of the project is to show the basic mechanisms of low-level quering to the database by `JdbcTemplate` in Spring.


# Dependencies

- spring-boot-starter-data-jdbc
- spring-boot-starter-jdbc
- spring-boot-starter-web
- h2

# Database Configuration

By default application uses an in-memory database (H2) which gets populated at startup with data. `spring.h2.console.enabled=true` is setup by default so h2 console is automatically exposed at `http://localhost:8080/h2-console`
and it is possible to inspect the content of the database using `jdbc:h2:mem:testdb` and username `sa`.

SQL scripts are under `resources` dir:

- `schema.sql` 
- `schema-data.sql` 

# Useful links
 
- https://docs.spring.io/spring-framework/docs/current/reference/html/data-access.html#jdbc