package pl.sda.springmaterials;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.jdbc.core.JdbcTemplate;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CustomJdbcTemplateCallTest {

    JdbcTemplate jdbcTemplate = Mockito.mock(JdbcTemplate.class);

    @Test
    public void whenMockJdbcTemplate_thenReturnCorrectEmployeeCount(){
        CustomJdbcTemplateCall jdbcTemplateCall = new CustomJdbcTemplateCall(jdbcTemplate);
        List<Map<String, Object>> mockResult = new ArrayList<>();
        Map<String, Object> mockValues = new HashMap<>();
        mockValues.put("EMPLOYEE_IDEMPLOYEE_ID", 100);
        mockValues.put("FIRST_NAME", "Steven");
        mockResult.add(mockValues);

        Mockito.when(jdbcTemplateCall.getAllEmployees()).thenReturn(mockResult);
        assertEquals(1, jdbcTemplateCall.getAllEmployees().size());
    }

}