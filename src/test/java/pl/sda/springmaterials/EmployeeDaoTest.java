package pl.sda.springmaterials;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import pl.sda.springmaterials.dao.EmployeeDao;
import pl.sda.springmaterials.mappers.EmployeeRowMapper;

class EmployeeDaoTest {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate = Mockito.mock(NamedParameterJdbcTemplate.class);


    @Test
    public void test(){
        EmployeeDao employeeDao = new EmployeeDao(namedParameterJdbcTemplate);
        String sql = "select * from employee";
        Mockito.when(namedParameterJdbcTemplate.query(sql, new EmployeeRowMapper()));

        System.out.println(employeeDao.getAll());
    }


}