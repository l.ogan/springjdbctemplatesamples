package pl.sda.springmaterials.mappers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.sda.springmaterials.model.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRowMapperTest {

    EmployeeRowMapper mapper = new EmployeeRowMapper();

    @Test
    public void testMapRow() throws SQLException {
        ResultSet resultSet = Mockito.mock(ResultSet.class);
        int rowNum = 1;
        Mockito.when(resultSet.getString("first_name")).thenReturn("first_name");
        Mockito.when(resultSet.getString("last_name")).thenReturn("last_name");

        Employee result = mapper.mapRow(resultSet, rowNum);

        assertEquals(result.getFirstName(), "first_name");
        assertEquals(result.getLastName(), "last_name");
    }
}