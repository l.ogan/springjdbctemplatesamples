package pl.sda.springmaterials;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.sda.springmaterials.dao.EmployeeDao;
import pl.sda.springmaterials.dao.JobsDao;
import pl.sda.springmaterials.mapper.EmployeeMapper;

@SpringBootApplication
@Slf4j
@AllArgsConstructor
public class SpringMaterialsApplication implements CommandLineRunner {

    private final CustomJdbcTemplateCall customJdbcTemplateCall;
    private final EmployeeMapper employeeMapper;
    private final EmployeeDao employeeDao;
    private final JobsDao jobsDao;

    public static void main(String[] args) {
        SpringApplication.run(SpringMaterialsApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info(customJdbcTemplateCall.getAllEmployees().toString());
    }
}
