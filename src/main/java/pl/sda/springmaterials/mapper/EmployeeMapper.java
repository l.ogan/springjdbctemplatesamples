package pl.sda.springmaterials.mapper;

import org.springframework.stereotype.Component;
import pl.sda.springmaterials.dto.EmployeeDTO;
import pl.sda.springmaterials.model.Employee;

import java.time.LocalDate;

@Component
public class EmployeeMapper {

    public EmployeeDTO toDto(Employee employee){
        Integer employeeId = employee.getEmployeeId();
        String firstName = employee.getFirstName();
        String lastName = employee.getLastName();
        String email = employee.getEmail();
        String phoneNumber = employee.getPhoneNumber();
        LocalDate hireDate = employee.getHireDate();
        Integer jobId = employee.getJobId();
        Double salary = employee.getSalary();
        Integer managerId = employee.getManagerId();
        Integer departmentId = employee.getDepartmentId();

        return new EmployeeDTO(
                employeeId, firstName,
                lastName, email,
                phoneNumber, hireDate,
                jobId, salary,
                managerId, departmentId
        );
    }
}
