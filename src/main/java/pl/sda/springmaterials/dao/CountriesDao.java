package pl.sda.springmaterials.dao;

import pl.sda.springmaterials.model.Countries;

import java.util.List;
import java.util.Optional;

public class CountriesDao implements DAO<Countries> {
    @Override
    public Optional<Countries> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Countries> getAll() {
        return null;
    }

    @Override
    public void save(Countries dependents) {

    }

    @Override
    public void update(Countries dependents, int id) {

    }

    @Override
    public void delete(int id) {

    }
}
