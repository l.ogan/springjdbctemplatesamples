package pl.sda.springmaterials.dao;

import pl.sda.springmaterials.model.Dependents;

import java.util.List;
import java.util.Optional;

public class DependentsDao implements DAO<Dependents> {
    @Override
    public Optional<Dependents> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Dependents> getAll() {
        return null;
    }

    @Override
    public void save(Dependents dependents) {

    }

    @Override
    public void update(Dependents dependents, int id) {

    }

    @Override
    public void delete(int id) {

    }
}
