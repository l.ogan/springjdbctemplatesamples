package pl.sda.springmaterials.dao;

import pl.sda.springmaterials.model.Locations;

import java.util.List;
import java.util.Optional;

public class LocationsDao implements DAO<Locations> {
    @Override
    public Optional<Locations> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Locations> getAll() {
        return null;
    }

    @Override
    public void save(Locations locations) {

    }

    @Override
    public void update(Locations locations, int id) {

    }

    @Override
    public void delete(int id) {

    }
}
