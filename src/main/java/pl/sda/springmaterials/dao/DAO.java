package pl.sda.springmaterials.dao;

import pl.sda.springmaterials.model.Employee;

import java.util.List;
import java.util.Optional;

public interface DAO<T> {
    Optional<T> get(long id);
    List<T> getAll();
    void save(T t);
    void update(T t, int id);
    void delete(int id);
}

