package pl.sda.springmaterials.dao;

import pl.sda.springmaterials.model.Regions;

import java.util.List;
import java.util.Optional;

public class RegionsDao implements DAO<Regions> {
    @Override
    public Optional<Regions> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Regions> getAll() {
        return null;
    }

    @Override
    public void save(Regions regions) {

    }

    @Override
    public void update(Regions regions, int id) {

    }

    @Override
    public void delete(int id) {

    }
}
