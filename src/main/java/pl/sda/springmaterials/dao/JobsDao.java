package pl.sda.springmaterials.dao;

import lombok.AllArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import pl.sda.springmaterials.exceptions.CustomSQLErrorCodeTranslator;
import pl.sda.springmaterials.model.Jobs;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Component
public class JobsDao implements DAO<Jobs> {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public Optional<Jobs> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Jobs> getAll() {
        return null;
    }

    @Override
    public void save(Jobs jobs) {

    }

    @Override
    public void update(Jobs jobs, int id) {

    }

    @Override
    public void delete(int id) {
        jdbcTemplate.setExceptionTranslator(new CustomSQLErrorCodeTranslator());
        String sql = "delete jobs where job_id = ?";
        Object[] params = {id};
        try {
            int rows = jdbcTemplate.update(sql, params);
            System.out.println("Rows deleted: " + rows);
        }catch (DataAccessException exception){
            exception.printStackTrace();
        }
    }
}
