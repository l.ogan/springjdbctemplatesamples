package pl.sda.springmaterials.dao;

import pl.sda.springmaterials.model.Departments;

import java.util.List;
import java.util.Optional;

public class DepartmentsDao implements DAO<Departments> {
    @Override
    public Optional<Departments> get(long id) {
        return Optional.empty();
    }

    @Override
    public List<Departments> getAll() {
        return null;
    }

    @Override
    public void save(Departments dependents) {

    }

    @Override
    public void update(Departments dependents, int id) {

    }

    @Override
    public void delete(int id) {

    }
}
