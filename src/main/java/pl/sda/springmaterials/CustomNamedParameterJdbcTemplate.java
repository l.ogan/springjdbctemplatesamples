package pl.sda.springmaterials;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Service;
import pl.sda.springmaterials.model.Employee;

import java.util.List;

@Service
public class CustomNamedParameterJdbcTemplate {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final RowMapper<Employee> employeeRowMapper = (resultSet, rowNum) -> {
        Employee employee = new Employee();
        employee.setFirstName(resultSet.getString("first_name"));
        employee.setLastName(resultSet.getString("last_name"));
        employee.setPhoneNumber(resultSet.getString("phone_number"));
        employee.setEmail(resultSet.getString("email"));
        return employee;
    };

    public CustomNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        System.out.println(this.getEmployeeByEmail("asd"));
    }

    public List<Employee> getEmployeeByEmail(String email){
        String sql = "select * from employees where email = :email";
        SqlParameterSource namedParameters = new MapSqlParameterSource("email", email);
        return namedParameterJdbcTemplate.query(sql, namedParameters, employeeRowMapper);
    }
}
