package pl.sda.springmaterials;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import pl.sda.springmaterials.model.Employee;

import java.util.List;
import java.util.Map;

@Service
public class CustomJdbcTemplateCall {

    private final JdbcTemplate jdbcTemplate;

    public CustomJdbcTemplateCall(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Map<String, Object>> getAllEmployees(){
        String query = "select * from employees";
        List<Map<String, Object>> results = jdbcTemplate.queryForList(query);

        return results;
    }
}
