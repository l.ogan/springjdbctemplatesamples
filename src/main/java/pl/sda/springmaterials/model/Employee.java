package pl.sda.springmaterials.model;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class Employee {
    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private LocalDate hireDate;
    private Integer jobId;
    private Double salary;
    private Integer managerId;
    private Integer departmentId;
}

